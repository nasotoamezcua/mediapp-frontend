import { PacienteService } from './../../_service/paciente.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Paciente } from 'src/app/_model/paciente';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  cantidad: number = 0;
  displayedColumns = ['idPaciente', 'nombres', 'apellidos', 'acciones'];
  dataSource: MatTableDataSource<Paciente>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private snackBar: MatSnackBar,
    private pacienteService: PacienteService
    ) { }

  ngOnInit() {

    /* 
      Este codigo no se ejecutara cada que se cargue la pagina.
      Este codigo se ejecutara cada que se haga un next a la variable:
      pacienteCambio, que esta declarada en la clase  PacienteService
    */

    this.pacienteService.mensajeCambio.subscribe(data =>{
        this.snackBar.open(data, 'AVISO', {
          duration: 2000
        });
      });

    this.pacienteService.pacienteCambio.subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;    
    })

    this.pacienteService.listarPageable(0,10).subscribe(data => {
      //console.log(data);
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
      //this.dataSource.paginator = this.paginator;      
    });

    /*
    this.pacienteService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;      
    });
    */

  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLocaleLowerCase();
  }

  eliminar(idPaciente : number){
    this.pacienteService.eliminar(idPaciente).subscribe(() =>{
      this.pacienteService.listar().subscribe(data =>{
        this.pacienteService.pacienteCambio.next(data);
        this.pacienteService.mensajeCambio.next('SE ELIMINO');
    })
  });
  }

  mostrarMas(e: any){
    this.pacienteService.listarPageable(e.pageIndex, e.pageSize).subscribe(data =>{
      //console.log(data);
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    })
  }

}
