import { switchMap } from 'rxjs/operators';
import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';



@Component({
  selector: 'app-paciente-dialogo',
  templateUrl: './paciente-dialogo.component.html',
  styleUrls: ['./paciente-dialogo.component.css']
})
export class PacienteDialogoComponent implements OnInit {

  paciente: Paciente;

  constructor(
    private dialogRef: MatDialogRef<PacienteDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Paciente,
    private pacienteService: PacienteService
  ) { }

  ngOnInit() {
    this.paciente = new Paciente();
    this.paciente.nombres = this.data.nombres;
    this.paciente.apellidos = this.data.apellidos;
    this.paciente.dni = this.data.dni;
    this.paciente.direccion = this.data.direccion;
    this.paciente.telefono = this.data.telefono;
    this.paciente.email = this.data.email;
  }

  cancelar(){
    this.dialogRef.close();
  }

  operar(){
    this.pacienteService.registarDialogo(this.paciente).pipe(switchMap(id =>{
      return this.pacienteService.listarPorId(id);
    })).subscribe(data =>{
      this.pacienteService.pacienteAddCambio.next(data);
      this.pacienteService.mensajeCambio.next('Paciente Registrado');
    });

    this.dialogRef.close();
  }

}
