import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  token: string;
  usuario: string;
  roles: string[];

  constructor() { }

  ngOnInit() {
    this.miPerfil();
  }

  miPerfil(){
    this.token = sessionStorage.getItem(environment.TOKEN_NAME);

    const helper = new JwtHelperService();
    let decodedToken = helper.decodeToken(this.token);
    //console.log(decodedToken);

    this.usuario = decodedToken.user_name;
    this.roles = decodedToken.authorities;
  }

}
