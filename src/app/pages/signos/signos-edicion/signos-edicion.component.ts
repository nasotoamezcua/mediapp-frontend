import { PacienteService } from './../../../_service/paciente.service';
import { map, switchMap } from 'rxjs/operators';
import { Paciente } from './../../../_model/paciente';
import { SignosService } from './../../../_service/signos.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Signos } from 'src/app/_model/signos';
import { MatDialog, MatSnackBar } from '@angular/material';
import { PacienteDialogoComponent } from '../../paciente/paciente-dialogo/paciente-dialogo.component';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  form : FormGroup;
  id: number;
  edicion: boolean;
  pacienteTitulo: string;

  maxFecha: Date = new Date();

  pacientes: Paciente[] = [];
  pacienteSeleccionado: Paciente;
  pacientesFiltrados: Observable<any[]>;
  myControlPaciente: FormControl = new FormControl();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private signosService: SignosService,
    private pacienteService: PacienteService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar

  ) { }

  ngOnInit() {

    this.pacienteService.pacienteAddCambio.subscribe(data =>{
      this.pacienteSeleccionado = data;
      this.myControlPaciente.setValue('');
      this.myControlPaciente.setValue(this.pacienteSeleccionado);
    });

    this.pacienteService.mensajeCambio.subscribe(data =>{
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')
    });

    this.route.params.subscribe((params :Params) =>{
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.pacienteTitulo = this.edicion ? 'Seleccionar Paciente' : 'Paciente';

    this.listarPacientes();
    this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(map( val => this.filtrarPacientes(val)) );
  }

  filtrarPacientes(val : any){   
    if (val != null && val.idPaciente > 0) {     
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {     
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  mostrarPaciente(val : Paciente){    
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e :any){
    this.pacienteSeleccionado = e.option.value;
  }

  listarPacientes(){
    this.pacienteService.listar().subscribe(data =>{
      this.pacientes = data;
    })
  }

  initForm(){
    if(this.edicion){
      
      this.signosService.listarPorId(this.id).subscribe(data =>{
        
        this.pacienteSeleccionado = data.paciente;
        this.myControlPaciente.setValue(this.pacienteSeleccionado);
        
        this.form = new FormGroup({
          'id': new FormControl(data.idSigno),
          'paciente' : this.myControlPaciente,
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)
        });
      });
    }
  }

  operar(){
    let signo = new Signos()
    signo.idSigno = this.form.value['id'];
    signo.fecha = this.form.value['fecha'];
    signo.temperatura = this.form.value['temperatura'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    signo.paciente = this.pacienteSeleccionado;

    if(this.edicion){
      this.signosService.modificar(signo).pipe(switchMap(() =>{
        return this.signosService.listar();
      })).subscribe(data =>{
        this.signosService.signoCambio.next(data);
        this.signosService.mensajeCambio.next('Se actualizo');
      });
    }else{
      this.signosService.registrar(signo).pipe(switchMap( () =>{
        return this.signosService.listar();
      } )).subscribe(data =>{
        this.signosService.signoCambio.next(data);
        this.signosService.mensajeCambio.next('Se registro');
      });
    }

    this.router.navigate(['signos']);

    console.log(signo);
    
  }

  abrirDialogo(){
    let paciente = new Paciente();
    this.dialog.open(PacienteDialogoComponent, {
      width: '250px',
      data: paciente
    });
  }

}
