import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { SignosService } from './../../_service/signos.service';
import { Signos } from './../../_model/signos';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  cantidad: number
  displayedColumns = ['idSigno', 'paciente', 'temperatura', 'pulso', 'ritmoRespiratorio', 'fecha', 'acciones'];
  dataSource: MatTableDataSource<Signos>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private snackBar: MatSnackBar,
    private signoService: SignosService,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.signoService.mensajeCambio.subscribe(data =>{
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.signoService.signoCambio.subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signoService.listarPageable(0,10).subscribe(data =>{
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

  filtrar(valor: string){
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  eliminar(idSigno: number){
    this.signoService.eliminar(idSigno).pipe(switchMap( ()=>{
      return this.signoService.listar();
    } )).subscribe(data =>{
      this.signoService.signoCambio.next(data);
      this.signoService.mensajeCambio.next('Se elimino');
    });
  }

  paginado(e :any){
    this.signoService.listarPageable(e.pageIndex, e.pageSize).subscribe(data =>{
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });

  }

}
